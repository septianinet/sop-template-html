var options = {
    chart: {
        type: 'donut'
    },
    series: [44, 55, 13, 33],
    labels: ['Apple', 'Mango', 'Orange', 'Watermelon'],
    legend: {
        position: 'bottom'
    }
}

var element = document.querySelector('.pie-chart')

var pieChart = new ApexCharts(element, options)

pieChart.render()